import { Component, OnInit, Input } from '@angular/core';
import { HistoryService } from 'src/app/services/history.service';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-listweather',
  templateUrl: './listweather.component.html',
  styleUrls: ['./listweather.component.css']
})
export class ListweatherComponent implements OnInit {
  clickEventSubscription: Subscription;

  @Input()
  pCityId: number = 0;
  public lstHistories: any = [];

  constructor(public historyservice: HistoryService, private sharedService: SharedService) {
    this.clickEventSubscription = this.sharedService.getClickEvent().subscribe(() => {
      this.LoadHistory(this.pCityId);
    });
  }

  ngOnInit(): void {
    this.LoadHistory(this.pCityId);
  }

  ngOnDestroy() {
    this.clickEventSubscription.unsubscribe();
  }

  LoadHistory(id: number): void {
    this.historyservice.gethistories(id).subscribe(data => {
      this.lstHistories = data;
    });
  }

}
