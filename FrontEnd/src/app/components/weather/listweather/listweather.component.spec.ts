import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListweatherComponent } from './listweather.component';

describe('ListweatherComponent', () => {
  let component: ListweatherComponent;
  let fixture: ComponentFixture<ListweatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListweatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListweatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
