import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { City } from 'src/app/models/City';
import { History } from 'src/app/models/History';
import { WeatherService } from 'src/app/services/weather.service';
import { CityService } from 'src/app/services/city.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  form: FormGroup;
  weather: History;
  city: City;
  ck_History: boolean = false;
  CityId: number = 0;
  loading: boolean = false;
  selectedCity: string[] = [];
  showToast: boolean = false;
  message: any;


  constructor(private formBuilder: FormBuilder, private weatherservice: WeatherService, public cityservice: CityService, private sharedService: SharedService) {
    this.form = this.formBuilder.group({ cityid: [0, [Validators.required]], ck_History: 0 });
    this.weather = { temp_celsius: "", feels_like_celsius: "", };
    this.city = { id: 0, name: "", country_Name: "", visible: false }
    this.message = { text: "", class: "" }
  }

  ngOnInit(): void {
    this.cityservice.getcitiesvisibles();
  }

  changeSelect() {
    this.message.text = "";
    this.CityId = this.form.get('cityid')?.value;
  }

  getWeather() {
    this.loading = true;
    this.CityId = this.form.get('cityid')?.value;
    this.ck_History = this.form.get('ck_History')?.value;

    const city: History = {
      city: this.form.get('cityid')?.value
    }

    this.weatherservice.getWeather(this.form.get('cityid')?.value).subscribe(data => {
      this.city.name = data.city?.name;
      this.city.country_Name = data.city?.country_Name;
      this.weather.temp_celsius = data.temp_celsius + " °C";
      this.weather.feels_like_celsius = data.feels_like_celsius + " °C";
    });


    if (this.ck_History) {
      this.weatherservice.saveweather(city).subscribe(
        data => {
          this.message.text = "Se ha agregado tu consulta al historico.";
          this.message.class = "success";
        },
        err => {
          if (err.status == 200) {
            this.message.text = "Se ha agregado tu consulta al historico.";
            this.message.class = "success";
          }
          else {
            this.message.text = "Se ha producido un error.";
            this.message.class = "danger";
          }
        }
      );
    }

    setTimeout(() => this.loadHistory(), 1000);

  }

  loadHistory() {
    this.sharedService.sendClickEvent();
    this.loading = false;
  }


}
