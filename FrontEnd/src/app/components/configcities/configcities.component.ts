import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { City } from 'src/app/models/City';
import { CityService } from 'src/app/services/city.service';
import { CountryService } from 'src/app/services/country.service';


@Component({
  selector: 'app-configcities',
  templateUrl: './configcities.component.html',
  styleUrls: ['./configcities.component.css']
})
export class ConfigcitiesComponent implements OnInit {
  form: FormGroup;
  //lstCities: City[] = [];
  County_ISO: string = '';
  loading: boolean = false;

  constructor(private formBuilder: FormBuilder, public cityservice: CityService, public countryservice: CountryService) {
    this.form = this.formBuilder.group({ county: [0, [Validators.required]] });
  }

  ngOnInit(): void {
    this.countryservice.loadcountries();
  }

  async loadcities() {
    this.loading = true;

    //setTimeout(() => {
    this.cityservice.lstCities = [];
    this.County_ISO = this.form.get('county')?.value;
    let response = await this.cityservice.getcities(this.County_ISO);
    this.loading = false;
    //}, 1000);

  }

  changeState(city: City) {
    city.visible = !city.visible;
    if (city.visible) {
      this.cityservice.showcity(city);
    }
    else {
      this.cityservice.hidecity(city);
    }


  }

}
