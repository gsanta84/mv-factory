import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigcitiesComponent } from './configcities.component';

describe('ConfigcitiesComponent', () => {
  let component: ConfigcitiesComponent;
  let fixture: ComponentFixture<ConfigcitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigcitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigcitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
