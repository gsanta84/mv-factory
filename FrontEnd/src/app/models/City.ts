export class City {
    id: number = 0;
    name?: string;
    country_Name?: string;
    visible: boolean = false;
}