import { City } from "./City";

export class History {
    city?: City;
    temp_celsius?: string;
    feels_like_celsius?: string;
}