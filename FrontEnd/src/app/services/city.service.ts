import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from 'src/app/models/City';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  baseUrl = environment.baseUrl;
  cities = 'api/cities/';
  public lstCities: any = [];

  constructor(private http: HttpClient) { }

  getcitiesvisibles() {
    return this.http.get<City>(this.baseUrl + this.cities).toPromise().then(
      data => {
        this.lstCities = data;
      });
  }

  getcities(country_iso: string) {
    return this.http.get<City>(this.baseUrl + this.cities + country_iso).toPromise().then(
      data => {
        this.lstCities = data;
      });
  }

  hidecity(city: City) {
    return this.http.delete<City>(this.baseUrl + this.cities + city.id).toPromise().then(
      data => {
        //console.log(data);
      }
    );
  }

  showcity(city: City) {
    return this.http.put<City>(this.baseUrl + this.cities + city.id, city).toPromise().then(
      data => {
        //console.log(data);
      }
    );
  }

}
