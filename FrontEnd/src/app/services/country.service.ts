import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from 'src/app/models/Country';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  baseUrl = environment.baseUrl;
  countries = 'api/countries/';
  public lstCountries: any = [];

  constructor(private http: HttpClient) { }

  async loadcountries() {
    return await this.http.get<Country>(this.baseUrl + this.countries).toPromise().then(
      data => {
        this.lstCountries = data;
      });
  }

}