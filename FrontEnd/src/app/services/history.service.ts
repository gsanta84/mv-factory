import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  baseUrl = environment.baseUrl;

  histories = 'api/Histories/';
  public lstHistories: any = [];

  constructor(private http: HttpClient) { }

  gethistories(id: number) {
    return this.http.get<any>(this.baseUrl + this.histories + id);
  }

}
