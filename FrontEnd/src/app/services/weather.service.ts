import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { History } from '../models/History';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  baseUrl = environment.baseUrl;
  weather = 'api/Weather/';

  constructor(private http: HttpClient) { }

  getWeather(city: number) {
    return this.http.get<any>(this.baseUrl + this.weather + city)
  }

  saveweather(city: History): Observable<any> {
    return this.http.post<History>(this.baseUrl + this.weather + city.city, city)
  }

}
