import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigcitiesComponent } from './components/configcities/configcities.component';
import { WeatherComponent } from './components/weather/weather.component';

const routes: Routes = [
  { path: '', component: WeatherComponent },
  { path: 'config', component: ConfigcitiesComponent },
  { path: 'weather', component: WeatherComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
