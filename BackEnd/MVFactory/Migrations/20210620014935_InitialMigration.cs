﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MVFactory.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    CountryISO = table.Column<string>(type: "varchar(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "History",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    City_Id = table.Column<int>(type: "int", nullable: false),
                    Temp = table.Column<string>(type: "varchar(10)", nullable: false),
                    Temp_Sensation = table.Column<string>(type: "varchar(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_History", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OpenWeatherCity",
                columns: table => new
                {
                    RecordNum = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id = table.Column<int>(type: "int", nullable: false),
                    name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    state = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    country = table.Column<string>(type: "varchar(2)", nullable: true),
                    visible = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OpenWeatherCity", x => x.RecordNum);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Country_CountryISO",
                table: "Country",
                column: "CountryISO");

            migrationBuilder.CreateIndex(
                name: "IX_History_City_Id",
                table: "History",
                column: "City_Id");

            migrationBuilder.CreateIndex(
                name: "IX_OpenWeatherCity_country",
                table: "OpenWeatherCity",
                column: "country");

            migrationBuilder.CreateIndex(
                name: "IX_OpenWeatherCity_id",
                table: "OpenWeatherCity",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_OpenWeatherCity_visible",
                table: "OpenWeatherCity",
                column: "visible");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "History");

            migrationBuilder.DropTable(
                name: "OpenWeatherCity");
        }
    }
}
