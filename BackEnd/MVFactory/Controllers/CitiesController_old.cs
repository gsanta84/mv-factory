﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVFactory.Models;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MVFactory.Data;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MVFactory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController_OLD : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CitiesController_OLD(ApplicationDbContext context)
        {
            _context = context;
        }

        
        [HttpGet("{CountryISO}")]
        public async Task<ActionResult<IEnumerable<OpenWeatherCity>>> GetCities(string CountryISO)
        {
            //Query joining cities and countries, order by name ascending
            var countries = await (
                from ci in _context.OpenWeatherCity
                join co in _context.Country
                on ci.country equals co.CountryISO
                where ci.country == CountryISO
                orderby ci.name ascending
                select new OpenWeatherCity
                {
                    id = ci.id,
                    name = ci.name,
                    country = ci.country,
                    Country_Name = co.Name,
                    visible = ci.visible
                }
                ).ToListAsync();

            return countries;
        }

        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OpenWeatherCity>>> GetCities()
        {
            //Query to get all visibles cities
            var countries = await (
                from ci in _context.OpenWeatherCity
                join co in _context.Country
                on ci.country equals co.CountryISO
                where ci.visible == true
                select new OpenWeatherCity
                {
                    id = ci.id,
                    name = ci.name,
                    country = ci.country,
                    Country_Name = co.Name
                }
                ).ToListAsync();

            return countries;
        }


        
        [HttpPut("{id}")]
        public async Task<ActionResult<IEnumerable<OpenWeatherCity>>> PutCity(int id)
        {
            try {
                //Get the city from the id
                var city = (
                    from c in _context.OpenWeatherCity
                    where c.id == id
                    select c
                    ).FirstOrDefault();

                //change the visibility
                city.visible = true;                
                _context.Entry(city).Property("visible").IsModified = true;

                //save the change
                await _context.SaveChangesAsync();

                return Ok("the city is visible.");
            }
            catch {
                return BadRequest("Error to create a new city.");
            }
        }

        
        [HttpDelete("{Id}")]
        public async Task<ActionResult<OpenWeatherCity>> DeleteCity(int id)
        {
            try
            {
                //Get the city from the id
                var city = (
                    from c in _context.OpenWeatherCity
                    where c.id == id
                    select c
                    ).FirstOrDefault();

                //change the visibility
                city.visible = false;
                _context.Entry(city).Property("visible").IsModified = true;

                //save the change
                await _context.SaveChangesAsync();
                return Ok("the city has been hidden.");
            }
            catch
            {
                return BadRequest("Error to hidden the city.");
            }
        }
    }
}
