﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        /// <summary>
        /// Only a Information Message
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<object>> Get()
        {
            try { 
            await Task.Delay(1000);
                return Ok("El aplicativo DEMO está está funcionando correctamente.");
            }
            catch (Exception ex){
                return BadRequest(ex.Message);
            }
        }
    }
}
