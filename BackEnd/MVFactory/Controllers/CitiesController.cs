﻿using Microsoft.AspNetCore.Mvc;
using MVFactory.Models;
using MVFactory.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly ICityRepository _cityRepository;
        public CitiesController(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository ??
                throw new ArgumentNullException(nameof(cityRepository));
        }

        /// <summary>
        /// Get all visibles cities 
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<OpenWeatherCity>>></returns>
        [HttpGet]
        public IActionResult GetCities()
        {
            var cities = _cityRepository.GetCities();
            return new JsonResult(cities);
        }

        /// <summary>
        /// List of Cities from Country
        /// </summary>
        /// <param name="CountryISO"></param>
        /// <returns>Task<ActionResult<IEnumerable<OpenWeatherCity>>></returns>
        [HttpGet("{CountryISO}")]
        public ActionResult<IEnumerable<OpenWeatherCity>> GetCities(string CountryISO)
        {
            Country country = new Country();
            country.CountryISO = CountryISO;
            var cities = _cityRepository.GetCities(country);
            return Ok(cities);
        }

        /// <summary>
        /// Change the visibility of the city to visible
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<IEnumerable<OpenWeatherCity>>></returns>
        [HttpPut("{id}")]
        public ActionResult SetShowCity(int id)
        {
            OpenWeatherCity city = new OpenWeatherCity();
            city.id = id;
            _cityRepository.ShowCity(city);
            return Ok(_cityRepository.Save());
        }

        /// <summary>
        /// Change the visibility of the city to hide
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public ActionResult SetHideCity(int id)
        {
            OpenWeatherCity city = new OpenWeatherCity();
            city.id = id;
            _cityRepository.HideCity(city);
            return Ok(_cityRepository.Save());
        }
    }
}
