﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVFactory.Models;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MVFactory.Data;
using Microsoft.EntityFrameworkCore;
using MVFactory.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MVFactory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryRepository _countryRepository;
        public CountriesController(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository ??
                throw new ArgumentNullException(nameof(countryRepository));
        }       
        

        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns>Task<ActionResult<IEnumerable<Country>>></returns>
        [HttpGet]
        public ActionResult<IEnumerable<Country>> Get()
        {
            return Ok(_countryRepository.GetCountries());
        }        
    }
}
