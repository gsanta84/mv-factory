﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVFactory.Models;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MVFactory.Data;
using Microsoft.EntityFrameworkCore;
using MVFactory.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MVFactory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoriesController : ControllerBase
    {
        private readonly IHistoryRepository _historyRepository;

        public HistoriesController(IHistoryRepository historyRepository)
        {
            _historyRepository = historyRepository;
        }

        /// <summary>
        /// Get the full history of a city
        /// </summary>
        /// <param name="CityId"></param>
        /// <returns>Task<ActionResult<IEnumerable<History>>></returns>
        [HttpGet("{CityId}")]
        public ActionResult<IEnumerable<History>> GetHistories(int CityId)
        {
            OpenWeatherCity city = new OpenWeatherCity();
            city.id = CityId;
            return Ok(_historyRepository.GetHistories(city));
        }

    }
}
