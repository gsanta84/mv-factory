﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVFactory.Models;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MVFactory.Data;
using MVFactory.Helper;
using Microsoft.Extensions.Configuration;
using MVFactory.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MVFactory.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _config;
        private readonly IWeatherRepository _weatherRepository;


        public WeatherController(ApplicationDbContext context, IConfiguration config, IWeatherRepository weatherRepository)
        {
            _config = config;
            _context = context;
            _weatherRepository = weatherRepository;
        }

        /// <summary>
        /// Get weather from API openweathermap
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<OpenWeatherResponse>></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<OpenWeatherResponse>> Get(int id)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    //Uri from openweathermap.org
                    client.BaseAddress = new Uri("http://api.openweathermap.org/");
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    var apiKey = _config.GetValue<string>("ApiKey:Key_openweathermap");

                    //Get the weather, the API is the string.
                    var response = await client.GetAsync($"/data/2.5/weather?id={id}&appid={apiKey}");
                    response.EnsureSuccessStatusCode();

                    //Convert response to ReadAsStringAsync
                    var stringResult = await response.Content.ReadAsStringAsync();
                    //Convert to json
                    var rawWeather = JsonConvert.DeserializeObject<OpenWeatherResponse>(stringResult);

                    //Create a new OpenWeatherResponse instance 
                    OpenWeatherResponse weather = new OpenWeatherResponse();
                    weather.Main = new Weather_Main();
                    weather.city = new OpenWeatherCity();
                    weather.Main.feels_like = rawWeather.Main.feels_like;
                    weather.Main.temp = rawWeather.Main.temp;
                    weather.temp_celsius = Convertion.K_to_C(rawWeather.Main.temp.ToString());
                    weather.feels_like_celsius = Convertion.K_to_C(rawWeather.Main.feels_like.ToString());
                    weather.city.name = rawWeather.name;
                    weather.city.Country_Name = ((Country)_context.Country.Where(c => c.CountryISO == rawWeather.sys.country).FirstOrDefault()).Name;

                    return weather;

                }
                catch (HttpRequestException httpRequestException)
                {
                    return BadRequest($"Error getting weather from OpenWeather: {httpRequestException.Message}");
                }
            }
        }

        /// <summary>
        /// Save the Weather to History
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task<ActionResult<OpenWeatherResponse>></returns>
        [HttpPost("{id}")]
        public async Task<ActionResult> saveWeather(int id)
        {            
            //Get the Weather of City
            ActionResult<OpenWeatherResponse> rawWeather = await Get(id);

            //Save Weather
            await _weatherRepository.saveWeather(rawWeather, id);

            return Ok("Weather inserted in history");               
        }

    }
}
