﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Helper
{
    public static class Convertion
    {
        /// <summary>
        /// Convert Kelvin to Celsius
        /// </summary>
        /// <param name="Temp"></param>
        /// <returns>Temp in Celsius</returns>
        public static string K_to_C(string Temp)
        {
            decimal K = 273.15M;
            return (decimal.Parse(Temp) - K).ToString();
        }
    }
}
