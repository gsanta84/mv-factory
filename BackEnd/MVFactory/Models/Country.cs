﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Models
{
    [Index(nameof(CountryISO))]    
    public class Country
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        [Display(Name = "País")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "varchar(2)")]
        public string CountryISO { get; set; }       

    }
}
