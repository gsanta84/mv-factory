﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVFactory.Models
{           
    [Index(nameof(id))]
    [Index(nameof(visible))]
    [Index(nameof(country))]
    public class OpenWeatherCity
        {            
            public int id { get; set; }
            [Display(Name = "Ciudad")]
            public string name { get; set; }
            public string state { get; set; }        
            
            [Column(TypeName = "varchar(2)")]
            public string country { get; set; }
            public bool visible { get; set; }
            [NotMapped]
            [Display(Name = "País")]
            public string Country_Name { get; set; }
            [Key]
            [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
            public int RecordNum { get; set; }
    }
    
}
