﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Models
{
    [Index(nameof(City_Id))]    
    public class History
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name ="Fecha")]
        public DateTime Date { get; set; }        
        [Required]
        [Column(TypeName = "int")]
        public int City_Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(10)")]
        [Display(Name = "Clima")]
        public string Temp { get; set; }
        [Required]
        [Column(TypeName = "varchar(10)")]
        [Display(Name = "Sensación Térmica")]
        public string Temp_Sensation { get; set; }        
        [NotMapped]        
        public OpenWeatherCity City { get; set; }
    }
}
