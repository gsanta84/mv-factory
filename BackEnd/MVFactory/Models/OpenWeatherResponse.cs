﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Models
{
    public class OpenWeatherResponse
    {
        public Weather_Main Main { get; set; }
        public Weather_Sys sys { get; set; }
        public string name { get; set; }

        public OpenWeatherCity city { get; set; }
        public string temp_celsius { get; set; }
        public string feels_like_celsius { get; set; }

    }

    public class Weather_Main
    {
            public float temp { get; set; }
            public float feels_like { get; set; }            
            
    }

    public class Weather_Sys { 
        public string country { get; set; }
    }


}
