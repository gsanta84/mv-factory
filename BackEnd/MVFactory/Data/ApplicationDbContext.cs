﻿using Microsoft.EntityFrameworkCore;
using MVFactory.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
            
        }
        
        public DbSet<History> History { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<OpenWeatherCity> OpenWeatherCity { get; set; }

        
    }
    
}
