﻿using Microsoft.EntityFrameworkCore;
using MVFactory.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Data
{
    public static class DbInitializer
    {

        /// <summary>
        /// Initialize DataBase.
        /// Seed data from Json file
        /// </summary>
        /// <param name="context"></param>
        public static void Initialize(ApplicationDbContext context) {

            //Get directory
            var dir = Environment.CurrentDirectory;
            bool inserted = false;

            //If no data (Cities) in DB then insert the data
            if (!context.OpenWeatherCity.Any()) {
                inserted = true;
                var cities = new List<OpenWeatherCity>();
                using (StreamReader r = new StreamReader(dir + @"\Json\city.list.json"))
                {
                    string json = r.ReadToEnd();
                    cities = JsonConvert.DeserializeObject<List<OpenWeatherCity>>(json);
                }

                foreach (var city in cities)
                    context.OpenWeatherCity.Add(city);
            }


            //If no data (Countries) in DB then insert the data
            if (!context.Country.Any())
            {
                inserted = true;
                var countries = new List<Country>();
                using (StreamReader r = new StreamReader(dir + @"\Json\country.list.json"))
                {
                    string json = r.ReadToEnd();
                    countries = JsonConvert.DeserializeObject<List<Country>>(json);
                }

                foreach (var country in countries)
                    context.Country.Add(country);
            }

            //If any data was inserted then save the changes.
            if (inserted) {
                context.Database.SetCommandTimeout(600);
                context.SaveChanges();                
            }

        }
    }
}
