﻿using Microsoft.AspNetCore.Mvc;
using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public interface IWeatherRepository
    {
        Task<bool> saveWeather(ActionResult<OpenWeatherResponse> city, int id);
    }
}
