﻿using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public interface IHistoryRepository
    {
        IEnumerable<History> GetHistories(OpenWeatherCity city);        
    }
}
