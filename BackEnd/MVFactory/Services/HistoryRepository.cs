﻿using MVFactory.Data;
using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public class HistoryRepository : IHistoryRepository
    {
        private readonly ApplicationDbContext _context;
        public HistoryRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<History> GetHistories(OpenWeatherCity city)
        {
            var histories = from h in _context.History
                            join ci in _context.OpenWeatherCity on h.City_Id equals ci.id
                            join co in _context.Country on ci.country equals co.CountryISO
                            where ci.id == city.id
                            orderby h.Id descending
                            select new History
                            {
                                Id = h.Id,
                                City = new OpenWeatherCity
                                {
                                    name = ci.name,
                                    country = co.Name
                                },
                                Temp = h.Temp + " °C",
                                Temp_Sensation = h.Temp_Sensation + " °C"
                            };

            return histories.ToList();
        }
    }
}
