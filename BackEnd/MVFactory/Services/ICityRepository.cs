﻿using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public interface ICityRepository
    {
        IEnumerable<OpenWeatherCity> GetCities(Country country);
        IEnumerable<OpenWeatherCity> GetCities();
        void ShowCity(OpenWeatherCity openWeatherCity);
        void HideCity(OpenWeatherCity openWeatherCity);
        bool Save();
    }
}
