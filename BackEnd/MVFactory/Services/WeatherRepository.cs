﻿using Microsoft.AspNetCore.Mvc;
using MVFactory.Data;
using MVFactory.Helper;
using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public class WeatherRepository : IWeatherRepository
    {
        private readonly ApplicationDbContext _context;

        public WeatherRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<bool> saveWeather(ActionResult<OpenWeatherResponse> city, int id)
        {
            try
            {
                History weather_to_save = new History();
                weather_to_save.City_Id = id;
                weather_to_save.Date = DateTime.Now;
                weather_to_save.Temp = Convertion.K_to_C(city.Value.Main.temp.ToString());
                weather_to_save.Temp_Sensation = Convertion.K_to_C(city.Value.Main.feels_like.ToString());

                //Add new element into the context
                _context.History.Add(weather_to_save);

                //Save the new element
                await _context.SaveChangesAsync();
                return true;
            }
            catch {
                return false;
            }
        }
    }
}
