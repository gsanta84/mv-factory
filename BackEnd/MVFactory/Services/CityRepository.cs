﻿using Microsoft.EntityFrameworkCore;
using MVFactory.Data;
using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public class CityRepository : ICityRepository
    {
        private readonly ApplicationDbContext _context;
        public CityRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _context.Database.SetCommandTimeout(300);
        }
        public void HideCity(OpenWeatherCity openWeatherCity)
        {
            if (openWeatherCity == null)
            {
                throw new ArgumentNullException(nameof(openWeatherCity));
            }

                //Get the city from the id
                var city = (
                    from c in _context.OpenWeatherCity
                    where c.id == openWeatherCity.id
                    select c
                    ).FirstOrDefault();

                //change the visibility
                city.visible = false;
                _context.Entry(city).Property("visible").IsModified = true;            
        }

        public IEnumerable<OpenWeatherCity> GetCities(Country country)
        {
            var countries = (
                from ci in _context.OpenWeatherCity
                join co in _context.Country
                on ci.country equals co.CountryISO
                where ci.country == country.CountryISO
                orderby ci.name ascending
                select new OpenWeatherCity
                {
                    id = ci.id,
                    name = ci.name,
                    country = ci.country,
                    Country_Name = co.Name,
                    visible = ci.visible
                }
                );
            return countries.ToList();
        }

        public IEnumerable<OpenWeatherCity> GetCities()
        {            
            var countries = (
                from ci in _context.OpenWeatherCity
                join co in _context.Country
                on ci.country equals co.CountryISO
                where ci.visible == true
                select new OpenWeatherCity
                {
                    id = ci.id,
                    name = ci.name,
                    country = ci.country,
                    Country_Name = co.Name
                }
                );
            return countries.ToList();
        }


        public void ShowCity(OpenWeatherCity openWeatherCity)
        {
            if (openWeatherCity == null) {
                throw new ArgumentNullException(nameof(openWeatherCity));
            }
            
            var city = (
                    from c in _context.OpenWeatherCity
                    where c.id == openWeatherCity.id
                    select c
                    ).FirstOrDefault();

            //change the visibility
            city.visible = true;
            _context.Entry(city).Property("visible").IsModified = true;            
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
