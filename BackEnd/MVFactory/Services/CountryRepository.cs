﻿using Microsoft.EntityFrameworkCore;
using MVFactory.Data;
using MVFactory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactory.Services
{
    public class CountryRepository : ICountryRepository
    {
        private readonly ApplicationDbContext _context;
        public CountryRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _context.Database.SetCommandTimeout(300);
            
        }

        public IEnumerable<Country> GetCountries()
        {            
           return _context.Country.OrderBy(c => c.Name).ToList();            
        }
    }
}
